package com.ema.reactor_usage.resource;

import reactor.core.publisher.Mono;
import java.util.List;

/**
 * Interface for the integers resource.
 */
public interface IntegersResource {
    Mono<List<Integer>> getIntegers();
}
