package com.ema.reactor_usage.resource;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import java.util.Arrays;
import java.util.List;

/**
 * Implementation for the cards resource.
 */
@Setter
@Slf4j
@RestController
@RequestMapping("/integers")
public class IntegersResourceImpl implements IntegersResource {

    @GetMapping
    @Override
    public Mono<List<Integer>> getIntegers() {
        Integer[] ints = new Integer[]{1, 2, 4, 8};
        List<Integer> integers = Arrays.asList(ints);
        return Mono.just(integers);
    }
}
