package com.ema.reactor_usage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaSpringReactorUsageApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaSpringReactorUsageApplication.class, args);
    }

}
